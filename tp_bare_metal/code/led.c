/* Fichier source pilotage LEDs */ 

#include "led.h" 
#define STM32L475xx
#include "stm32l4xx.h"

void led_init() {

	/* Active l'horloge du port B et C */
	SET_BIT(RCC->AHB2ENR, RCC_AHB2ENR_GPIOBEN) ;
	SET_BIT(RCC->AHB2ENR, RCC_AHB2ENR_GPIOCEN) ;

	/* Passe la broche PB14 en mode sortie */
	MODIFY_REG(GPIOB->MODER, GPIO_MODER_MODE14, GPIO_MODER_MODE14_0) ;  
};

void led_g_on(){

	SET_BIT(GPIOB->BSRR, GPIO_BSRR_BS14) ;
	
} ;
	
void led_g_off(){

	SET_BIT(GPIOB->BSRR, GPIO_BSRR_BR14) ;
} ;

void led(enum state state){

	/* Passe la broche 9 en mode entrée, éteins les LEDs */
	if (state == LED_OFF) {
		MODIFY_REG(GPIOC->MODER, GPIO_MODER_MODE9, 0) ;
	}

	/* Passe la broche 9 en mode sortie à l'état haut, LED jaune */
	if (state == LED_YELLOW) {
		MODIFY_REG(GPIOC->MODER, GPIO_MODER_MODE9, GPIO_MODER_MODE9_0) ;
		SET_BIT(GPIOC->BSRR, GPIO_BSRR_BS9) ;
	}
	
	/* Passe la broche 9 en mode sortie à l'état bas, LED verte */
	if (state == LED_BLUE) {
		MODIFY_REG(GPIOC->MODER, GPIO_MODER_MODE9, GPIO_MODER_MODE9_0) ; 
		SET_BIT(GPIOC->BSRR, GPIO_BSRR_BR9) ;	
	}
}
