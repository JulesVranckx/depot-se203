#include <stdint.h>

extern uint8_t _bstart, _bend, _edata, _lma_data, _data, _text, _etext, _lma_text, _vtor, _evtor, _lma_vtor ;



void init(){

	uint8_t *ptr;

	for (ptr = &_bstart; ptr < &_bend; ptr++){
		*ptr = 0;
	}

	uint8_t* crrt_data = &_lma_data ;
	
	for (ptr = &_data; ptr < &_edata; ptr++){
		*ptr = *crrt_data ;
		crrt_data++ ;
	}

	uint8_t * crrt_text = &_lma_text ;

	for (ptr = &_text; ptr < &_etext; ptr ++){
		* ptr = *crrt_text ;
		crrt_text++ ;
	}

	uint8_t * crrt_vtor = &_lma_vtor ;

	for (ptr = &_vtor; ptr < &_evtor; ptr++){
		*ptr = *crrt_vtor ;
		crrt_vtor++ ;
	}
}
