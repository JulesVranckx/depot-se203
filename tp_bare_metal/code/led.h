#ifndef _LED_H 
#define _LED_H


enum state {LED_OFF, LED_YELLOW, LED_BLUE} ;

/* Active l'horloge du port B */
void led_init(void) ;

/*Allume la LED */
void led_g_on(void) ;

/*Eteins la LED */
void led_g_off(void) ;

/* Pilote les LEDs 3 et 4 */
void led(enum state) ;

#endif // _LED_H
