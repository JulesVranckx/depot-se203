	.syntax unified
	.global _start
	.cpu cortex-m4
	.thumb

	.thumb_func
_start :
	ldr sp,=_stack_address
	bl init
	bl main

_exit :
	b _exit
	

