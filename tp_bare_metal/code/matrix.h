/* Headers de matrix.c */ 

#ifndef _MATRIX_H
#define _MATRIX_H
#include <stdint.h>
#include <stddef.h>

void matrix_init(void) ;
void pulse_SCK(void);
void pulse_LAT(void);
void deactivate_rows(void);
void activate_row(int);
void send_bytes(uint8_t, int);

typedef struct {
	uint8_t r;
	uint8_t g;
	uint8_t b;
} rgb_color ;

void mat_set_row(int,rgb_color *);

void init_bank0() ;

void test_pixels() ;

void affiche_image(volatile uint8_t*);

extern volatile uint8_t frame[192] ;
#endif //_MATRIX_H
