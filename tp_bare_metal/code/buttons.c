/* Contrôle des boutons */ 
#include "buttons.h"
#define STM32L475xx
#include "stm32l4xx.h"
#include "main.h"
#include "led.h"

void button_init(){

	/* Active l'horloge du port C */
	SET_BIT(RCC->AHB2ENR, RCC_AHB2ENR_GPIOCEN) ;
 
        /* Passe la broche PB13 en mode entrée */
	MODIFY_REG(GPIOC->MODER, GPIO_MODER_MODE13, 0) ; 

	/* Configuration de l'EXTI */ 
	SET_BIT(EXTI->IMR1, EXTI_IMR1_IM13) ;
	CLEAR_BIT(EXTI->RTSR1, EXTI_RTSR1_RT13) ;
	SET_BIT(EXTI->FTSR1, EXTI_FTSR1_FT13) ;

	/* Selection de la broche PC13 pour source d'IRQ pour EXTI13 */
	MODIFY_REG(SYSCFG->EXTICR[3], SYSCFG_EXTICR4_EXTI13_Msk, SYSCFG_EXTICR4_EXTI13_PC) ;

	/* Active l'interruption */ 
	NVIC_EnableIRQ(40) ;
	
}

void EXTI15_10_IRQHandler() {
	
	SET_BIT(EXTI->PR1, EXTI_PR1_PIF13) ;

	GPIOB->ODR ^= GPIO_ODR_OD14 ;
}
