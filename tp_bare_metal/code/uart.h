/* Headers de uart.c */ 

#ifndef _UART_H
#define _UART_H
#include <stdint.h>
#include <stddef.h>

/* Initialise l'UART */ 
void init_uart(int) ; 

void uart_putchar(uint8_t) ;

uint8_t uart_getchar(void) ;

void uart_puts(const uint8_t *) ;

void uart_gets(uint8_t * s, size_t size) ;

void USART1_IRQHandler(void) ;

#endif // _UART_H


