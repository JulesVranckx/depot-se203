/* Fichier de configuration de l'USART */

#include "uart.h"
#define STM32L475xx
#include "stm32l4xx.h"
#include "matrix.h"

int frame_crrt_pos = 0 ;
int ready = 0 ;

void init_uart(int baudrate){

	SET_BIT(RCC->AHB2ENR, RCC_AHB2ENR_GPIOBEN) ;

	/* Broche 6 (TX) en mode Alternative */
	MODIFY_REG(GPIOB->MODER, GPIO_MODER_MODE6, GPIO_MODER_MODE6_1) ;

	/* Broche 7 (RX) en mode Alternative */
	MODIFY_REG(GPIOB->MODER, GPIO_MODER_MODE7, GPIO_MODER_MODE7_1) ;

	/* Bon numéro de fonctions dans AFRL */
	MODIFY_REG(GPIOB->AFR[0], GPIO_AFRL_AFSEL6, (0x7 << 24)) ;
	MODIFY_REG(GPIOB->AFR[0], GPIO_AFRL_AFSEL7, (0x7 << 28)) ;

	/* USART1 clock enable */
	SET_BIT(RCC->APB2ENR, RCC_APB2ENR_USART1EN) ;

	/* USART1 clock is PCLK */
	MODIFY_REG(RCC->CCIPR, RCC_CCIPR_USART1SEL, 0) ;

	/* Reset du port série */
	SET_BIT(RCC->APB2RSTR, RCC_APB2RSTR_USART1RST) ;
	CLEAR_BIT(RCC->APB2RSTR, RCC_APB2RSTR_USART1RST) ;

	/* On désactive l'UART avant de la paramétrer */
	CLEAR_BIT(USART1->CR1, USART_CR1_UE) ;

	/* On définit la bonne vitesse du port série */
	USART1->BRR = 80000000 / baudrate ; 
	/* On initialise CR1 et CR2 à 0 */ 
	USART1->CR1 = 0 ;
	USART1->CR2 = 0 ;

	/* On choisit un oversampling de 16 */
	CLEAR_BIT(USART1->CR1, USART_CR1_OVER8) ;

	/* On choisit de prendre 8 bits de données */ 
	CLEAR_BIT(USART1->CR1, USART_CR1_M0) ;
	CLEAR_BIT(USART1->CR1, USART_CR1_M1) ;

	/* 1 bit de stop */ 
	CLEAR_BIT(USART1->CR2, USART_CR2_STOP_0) ;
	CLEAR_BIT(USART1->CR2, USART_CR2_STOP_1) ;

	/* Pas de contrôle de parité */
	CLEAR_BIT(USART1->CR1, USART_CR1_PCE) ;				

	/* Activatinon de l'UART */ 
	SET_BIT(USART1->CR1, USART_CR1_UE) ;

	/* Activation du transmetteur */ 
	SET_BIT(USART1->CR1, USART_CR1_TE) ;

	/* Activation du récepteur */ 
	SET_BIT(USART1->CR1, USART_CR1_RE) ;

	NVIC_EnableIRQ(37) ;

	SET_BIT(USART1->CR1, USART_CR1_RXNEIE);
}

void uart_putchar(uint8_t c) { 

	/* Envoie le caractère c sur l'UART */

	while ((USART1->ISR & USART_ISR_TXE) == 0) ;
	
	MODIFY_REG(USART1->TDR, USART_TDR_TDR, c) ;

}

uint8_t uart_getchar(void) {

	/* Attend de recevoir un caractère de l'UART */

	while ((USART1->ISR & USART_ISR_RXNE) == 0) ;

	return (USART1->RDR & USART_RDR_RDR) ;
}

void uart_puts(const uint8_t * s) {

	/* Envoie la chaine de caractère s sur l'UART */ 

	while (*s != '\0') {
		uart_putchar(*s) ;
		s++ ;
	}
}

void uart_gets(uint8_t * s, size_t size) {
	
	/* Lis une chaine de caractère de taille au plus size et l'écrit à l'adresse pointée par s */ 

	uint8_t * current = s ;
 
	for (unsigned int i = 0 ; i < size ; i++) {
		*current  = uart_getchar() ;
		if (*current == '\r' || *current == '\n') {current++ ; break ;} 
		else { current++ ;}  
	}
	
	*current = '\0' ; 
}

void USART1_IRQHandler(){

	uint8_t c;
	
	if ((USART1->ISR & USART_ISR_FE) | (USART1->ISR & USART_ISR_ORE)){

		ready = 0 ;
		for (int i = 0; i < 192; i++){
			frame[i] = 0 ;
		}
		SET_BIT(USART1->ICR, USART_ICR_ORECF | USART_ICR_FECF);
		return ;	
	}

	if ((USART1->ISR & USART_ISR_RXNE)){
		
		c = READ_BIT(USART1->RDR, 0xff) ; 

		if (c == 0xff) { 
			ready = 1 ;
			frame_crrt_pos = 0 ;
		}
		else { 
			if (ready) {
				frame[frame_crrt_pos]  = c ;
				frame_crrt_pos++;
				if (frame_crrt_pos == 192) {
					ready = 0 ;	
				} 
		 	}
			else {return ;}
		}
		
			
	}
}
