#include "clocks.h"
#include "led.h"
#include "uart.h"
#include "matrix.h"
#include "irq.h"
#include "buttons.h"
#include <stdint.h>

#define WAIT_TIME 2000000

uint32_t to_be_verified ;

void wait(int n) {
	for (int i = 0; i < n; i++) 
		asm volatile ("nop") ;
}
	
int main(){

	clocks_init() ;
	led_init() ;
	matrix_init();
	init_uart(38400) ;
	irq_init();
	button_init() ;
	while(1) {affiche_image(frame);}
	return 0 ;

}

