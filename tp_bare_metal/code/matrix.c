/* Controleur de la matrice de LEDs */

#include "matrix.h"
#define STM32L475xx
#include "stm32l4xx.h"
#include "main.h"
 /*static void RST(int x) {
	
	if (x == 0) {SET_BIT(GPIOC->BSRR, GPIO_BSRR_BR3) ;}
	else if (x == 1) {SET_BIT(GPIOC->BSRR, GPIO_BSRR_BS3) ;}
} */

static void SB(int x) {
	
	if (x == 0) { SET_BIT(GPIOC->BSRR, GPIO_BSRR_BR5) ;}
	else { SET_BIT(GPIOC->BSRR, GPIO_BSRR_BS5) ;}
} 

static void LAT(int x) {

	if (x == 0) { SET_BIT(GPIOC->BSRR, GPIO_BSRR_BR4) ;}
 	else { SET_BIT(GPIOC->BSRR, GPIO_BSRR_BS4) ;}
}

static void SCK(int x) {

	if (x == 0) { SET_BIT(GPIOB->BSRR, GPIO_BSRR_BR1) ;}
 	else { SET_BIT(GPIOB->BSRR, GPIO_BSRR_BS1) ;}
}

static void SDA(int x) {

	if (x == 0) { SET_BIT(GPIOA->BSRR, GPIO_BSRR_BR4) ;}
 	else { SET_BIT(GPIOA->BSRR, GPIO_BSRR_BS4) ;}
}


static void ROW0(int x) {

	if (x == 0) { SET_BIT(GPIOB->BSRR, GPIO_BSRR_BR2) ;}
 	else { SET_BIT(GPIOB->BSRR, GPIO_BSRR_BS2) ;}
}

static void ROW1(int x) {

	if (x == 0) { SET_BIT(GPIOA->BSRR, GPIO_BSRR_BR15) ;}
 	else { SET_BIT(GPIOA->BSRR, GPIO_BSRR_BS15) ;}
}

static void ROW2(int x) {

	if (x == 0) { SET_BIT(GPIOA->BSRR, GPIO_BSRR_BR2) ;}
	else { SET_BIT(GPIOA->BSRR, GPIO_BSRR_BS2) ;}
}

static void ROW3(int x) {

	if (x == 0) { SET_BIT(GPIOA->BSRR, GPIO_BSRR_BR7) ;}
	else { SET_BIT(GPIOA->BSRR, GPIO_BSRR_BS7) ;}
}

static void ROW4(int x) {

	if (x == 0) { SET_BIT(GPIOA->BSRR, GPIO_BSRR_BR6) ;}
	else { SET_BIT(GPIOA->BSRR, GPIO_BSRR_BS6) ;}
}

static void ROW5(int x) {

	if (x == 0) { SET_BIT(GPIOA->BSRR, GPIO_BSRR_BR5) ;}
	else { SET_BIT(GPIOA->BSRR, GPIO_BSRR_BS5) ;}
}

static void ROW6(int x) {

	if (x == 0) { SET_BIT(GPIOB->BSRR, GPIO_BSRR_BR0) ;}
	else { SET_BIT(GPIOB->BSRR, GPIO_BSRR_BS0) ;}
}

static void ROW7(int x) {

	if (x == 0) { SET_BIT(GPIOA->BSRR, GPIO_BSRR_BR3) ;}
	else { SET_BIT(GPIOA->BSRR, GPIO_BSRR_BS3) ;}
}

void pulse_SCK(){
	
	/* Réalise un pulse positif sur SCK (bas, attente, haut, attente, bas, attente) */ 

	SCK(0) ;
	

	SCK(1) ;
	

	SCK(0) ;
	

}

void pulse_LAT(){

	/* Réalise une pulse négatif sur LAT (haut, attente, bas, attente, haut, attente) */

	LAT(1) ;

	LAT(0) ;

	LAT(1) ;

}

void deactivate_rows(){
	
	/* Eteins toutes les lignes */ 
	
	ROW0(0) ;
	ROW1(0) ;
	ROW2(0) ;
	ROW3(0) ;
	ROW4(0) ;
	ROW5(0) ;
	ROW6(0) ;
	ROW7(0) ;

}

void activate_row(int row){
	
	/* Allume la ligne row */ 

	switch (row) {

		case 0 : ROW0(1) ; break ;
		case 1 : ROW1(1) ; break ;
		case 2 : ROW2(1) ; break ;
		case 3 : ROW3(1) ; break ;
		case 4 : ROW4(1) ; break ;
		case 5 : ROW5(1) ; break ;
		case 6 : ROW6(1) ; break ;
		case 7 : ROW7(1) ; break ;
		default : break ;
	}
}

void send_byte(uint8_t val, int bank){

	/* Envoie 8 bits consécutifs au bank bank du DM163 */ 

	/* Sélection du BANK */ 
	SB(bank) ;
	

	for (int i = 7; i >= 0 ; i--) {

		SDA( val & (1 << i)) ;
		pulse_SCK() ;
	}

}

void mat_set_row(int row, rgb_color* val){

	/* Active la ligne row avec les couleurs contenues dans val */ 
	
	for (int i = 7 ; i >= 0 ; i--) {
		
		send_byte(val[i].b, 1) ;	
		send_byte(val[i].g, 1) ;
		send_byte(val[i].r, 1) ;
	}	
	
	deactivate_rows() ;
	for (int i = 0; i < 100; i++) {
		asm volatile("nop") ;
	}
	pulse_LAT();
	activate_row(row);

}

void init_bank0() {

	for (int i = 0; i < 18; i++) {
		send_byte(0xff,0);
	}
	pulse_LAT();		
}

void matrix_init() {

	/* Activation des ports A, B et C */

	SET_BIT(RCC->AHB2ENR, RCC_AHB2ENR_GPIOAEN) ;	
	SET_BIT(RCC->AHB2ENR, RCC_AHB2ENR_GPIOBEN) ;
	SET_BIT(RCC->AHB2ENR, RCC_AHB2ENR_GPIOCEN) ;

	/* Configuration des branches reliées en mode Output haute vitesse */ 

	/* PC5 */ 
	MODIFY_REG(GPIOC->MODER, GPIO_MODER_MODE5, GPIO_MODER_MODE5_0) ;
	MODIFY_REG(GPIOC->OSPEEDR, GPIO_OSPEEDR_OSPEED5, GPIO_OSPEEDR_OSPEED5_1) ;
	
	/* PC4 */
	MODIFY_REG(GPIOC->MODER, GPIO_MODER_MODE4, GPIO_MODER_MODE4_0) ;
	MODIFY_REG(GPIOC->OSPEEDR, GPIO_OSPEEDR_OSPEED4, GPIO_OSPEEDR_OSPEED4_1) ;

	/* PC3 */
	MODIFY_REG(GPIOC->MODER, GPIO_MODER_MODE3, GPIO_MODER_MODE3_0) ;
	MODIFY_REG(GPIOC->OSPEEDR, GPIO_OSPEEDR_OSPEED3, GPIO_OSPEEDR_OSPEED3_1) ;

	/* PB1 */
	MODIFY_REG(GPIOB->MODER, GPIO_MODER_MODE1, GPIO_MODER_MODE1_0) ;
	MODIFY_REG(GPIOB->OSPEEDR, GPIO_OSPEEDR_OSPEED1, GPIO_OSPEEDR_OSPEED1_1) ;

	/* PA4 */
	MODIFY_REG(GPIOA->MODER, GPIO_MODER_MODE4, GPIO_MODER_MODE4_0) ;
	MODIFY_REG(GPIOA->OSPEEDR, GPIO_OSPEEDR_OSPEED4, GPIO_OSPEEDR_OSPEED4_1) ;

	/* PB2 */
	MODIFY_REG(GPIOB->MODER, GPIO_MODER_MODE2, GPIO_MODER_MODE2_0) ;
	MODIFY_REG(GPIOB->OSPEEDR, GPIO_OSPEEDR_OSPEED2, GPIO_OSPEEDR_OSPEED2_1) ;

	/* PA15 */
	MODIFY_REG(GPIOA->MODER, GPIO_MODER_MODE15, GPIO_MODER_MODE15_0) ;
	MODIFY_REG(GPIOA->OSPEEDR, GPIO_OSPEEDR_OSPEED15, GPIO_OSPEEDR_OSPEED15_1) ;

	/* PA2 */
	MODIFY_REG(GPIOA->MODER, GPIO_MODER_MODE2, GPIO_MODER_MODE2_0) ;
	MODIFY_REG(GPIOA->OSPEEDR, GPIO_OSPEEDR_OSPEED2, GPIO_OSPEEDR_OSPEED2_1) ;

	/* PA7 */
	MODIFY_REG(GPIOA->MODER, GPIO_MODER_MODE7, GPIO_MODER_MODE7_0) ;
	MODIFY_REG(GPIOA->OSPEEDR, GPIO_OSPEEDR_OSPEED7, GPIO_OSPEEDR_OSPEED7_1) ;

	/* PA6 */
	MODIFY_REG(GPIOA->MODER, GPIO_MODER_MODE6, GPIO_MODER_MODE6_0) ;
	MODIFY_REG(GPIOA->OSPEEDR, GPIO_OSPEEDR_OSPEED6, GPIO_OSPEEDR_OSPEED6_1) ;

	/* PA5 */
	MODIFY_REG(GPIOA->MODER, GPIO_MODER_MODE5, GPIO_MODER_MODE5_0) ;
	MODIFY_REG(GPIOA->OSPEEDR, GPIO_OSPEEDR_OSPEED5, GPIO_OSPEEDR_OSPEED5_1) ;	

	/* PB0 */
	MODIFY_REG(GPIOB->MODER, GPIO_MODER_MODE0, GPIO_MODER_MODE0_0) ;
	MODIFY_REG(GPIOB->OSPEEDR, GPIO_OSPEEDR_OSPEED0, GPIO_OSPEEDR_OSPEED0_1) ;

	/* PA3 */
	MODIFY_REG(GPIOA->MODER, GPIO_MODER_MODE3, GPIO_MODER_MODE3_0) ;
	MODIFY_REG(GPIOA->OSPEEDR, GPIO_OSPEEDR_OSPEED3, GPIO_OSPEEDR_OSPEED3_1) ;

	/* Positionnement des valeurs initiales */ 

	/* Reset DM163 */
	SET_BIT(GPIOC->BSRR, GPIO_BSRR_BR3) ;

	/* LAT : 1 */ 
	SET_BIT(GPIOC->BSRR, GPIO_BSRR_BS4) ;		

	/* SB : 1 */ 
	SET_BIT(GPIOC->BSRR, GPIO_BSRR_BS5) ;

	/* SCK : 0 */
	SET_BIT(GPIOB->BSRR, GPIO_BSRR_BR1) ;

	/* SDA : 0 */ 
	SET_BIT(GPIOA->BSRR, GPIO_BSRR_BR4) ;

	/* C0 à C7 : 0 */ 
	SET_BIT(GPIOB->BSRR, GPIO_BSRR_BR2) ;
	SET_BIT(GPIOA->BSRR, GPIO_BSRR_BR15) ;
	SET_BIT(GPIOA->BSRR, GPIO_BSRR_BR2) ;
	SET_BIT(GPIOA->BSRR, GPIO_BSRR_BR7) ;
	SET_BIT(GPIOA->BSRR, GPIO_BSRR_BR6) ;
	SET_BIT(GPIOA->BSRR, GPIO_BSRR_BR5) ;
	SET_BIT(GPIOB->BSRR, GPIO_BSRR_BR0) ;
	SET_BIT(GPIOA->BSRR, GPIO_BSRR_BR3) ;

	/* On repasse RST à l'état haut */ 
	for (int i = 0; i < 8000000 ; i++) {
		asm volatile("nop");
	}
	SET_BIT(GPIOC->BSRR, GPIO_BSRR_BS3) ;
	
	init_bank0() ;

} 

	
/* Fonction de test */ 

void test_pixels() {

	 	rgb_color color[8];
		int row = 0 ;
		while(1) {
			
			for (int j = 0; j < 255; j++){
				for (int k = 0; k < 8; k++) {color[k].r = j ; color[k].g = 0; color[k].b = 0;}
				mat_set_row(row, color);
				wait(50000) ;
			}
			for (int j = 0; j < 255; j++){
				for (int k = 0; k < 8; k++) {color[k].b = j;color[k].g = 0; color[k].r = 0;}
				mat_set_row(row,color);
				wait(50000) ;
			}
			for (int j = 0; j < 255; j++){
				for (int k = 0; k < 8; k++) {color[k].g = j;color[k].r= 0; color[k].b = 0;}
				mat_set_row(row,color);
				wait(50000) ;
			} 
		row = (row + 1)%8 ;
		}
}

volatile uint8_t frame[192] = {0};

void affiche_image( volatile uint8_t  * picture){

	rgb_color image[8] ;
	for (int i = 0; i < 8; i++){
	
		for (int j = 0; j < 8; j++){

			image[j].r = *picture;
			picture++ ;
			image[j].g = *picture ;
			picture++ ; 
			image[j].b = *picture ;
			picture++ ;
		}
	mat_set_row(i, image) ;
	wait(10) ;
	}	

}

