#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

int32_t x = 34;
int32_t y;
const char mesg[] = "Hello World!\n";

int main(){

	printf("Adresse de x (section data) :%p\n", &x) ;
	printf("Adresse de y (section bss) : %p\n", &y) ;
	printf("Adresse de mesg (section rodata) :%p\n", &mesg) ;

	int z, zz ;

	printf("Adresse de z (section pile) :%p\n",&z) ;
	printf("Adresse de zz (pile) :%p\n", &zz) ;
	int * t = (int *) malloc(sizeof(int)) ; 

	printf("Valeur de t (section tas) :%p\n", t) ;

	return 0 ;

}

